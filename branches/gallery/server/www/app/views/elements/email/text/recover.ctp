Hello <?php echo $username; ?>,

You (or someone using your e-mail address) has requested to reset the
password on your account. You can reset your account password by
visiting the following URL:

<?php echo $reset_url; ?>


Kind regards,

-- 
The ODF-Shots team
<?php echo $base_url; ?>
