<?php
	$html->css('wmd', null, array(), false);
	$javascript->link('showdown', false);
	$javascript->link('wmd', false);
?>

<div class="galleries form">
<?php echo $form->create('Gallery');?>
	<fieldset>
 		<legend><?php $this->action == 'add' ? __('Add gallery') : __('Edit gallery');?></legend>
		<?php echo $form->input('id');?>
		<?php echo $form->input('name');?>
		<?php echo $form->input('group_id', array('label' => __('Who can edit this gallery', true), 'empty' => __('Only me', true)));?>
		<div id="wmd-button-bar"></div>
		<?php echo $form->input('description', array('id' => 'wmd-input'));?>
		<div id="wmd-preview" class="polaroid"></div>
	</fieldset>
	<?php echo $form->end('Submit');?>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Gallery.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Gallery.id'))); ?></li>
	</ul>
</div>
