<div class="platforms form">
<?php echo $form->create('Platform');?>
	<fieldset>
 		<legend><?php __('Add Platform');?></legend>
	<?php
		echo $form->input('name');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Platforms', true), array('action'=>'index'));?></li>
	</ul>
</div>
