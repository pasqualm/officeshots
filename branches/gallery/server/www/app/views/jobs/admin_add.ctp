<div class="jobs form">
<?php echo $form->create('Job');?>
	<fieldset>
 		<legend><?php __('Add Job');?></legend>
	<?php
		echo $form->input('request_id');
		echo $form->input('platform_id');
		echo $form->input('application_id');
		echo $form->input('version');
		echo $form->input('result_id');
		echo $form->input('factory_id');
		echo $form->input('locked');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Jobs', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Requests', true), array('controller'=> 'requests', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Request', true), array('controller'=> 'requests', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Applications', true), array('controller'=> 'applications', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Application', true), array('controller'=> 'applications', 'action'=>'add')); ?> </li>
	</ul>
</div>
