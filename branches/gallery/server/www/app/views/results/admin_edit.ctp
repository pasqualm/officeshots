<div class="results form">
<?php echo $form->create('Result');?>
	<fieldset>
 		<legend><?php __('Edit Result');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('factory_id');
		echo $form->input('format_id');
		echo $form->input('filename');
		echo $form->input('path');
		echo $form->input('mimetype_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action'=>'delete', $form->value('Result.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('Result.id'))); ?></li>
		<li><?php echo $html->link(__('List Results', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Factories', true), array('controller'=> 'factories', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Factory', true), array('controller'=> 'factories', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Formats', true), array('controller'=> 'formats', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Format', true), array('controller'=> 'formats', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Mimetypes', true), array('controller'=> 'mimetypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Mimetype', true), array('controller'=> 'mimetypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
