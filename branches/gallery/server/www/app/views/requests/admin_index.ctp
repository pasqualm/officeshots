<div class="requests index">
<h2><?php __('Requests');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('ip_address');?></th>
	<th><?php echo $paginator->sort('doctype_id');?></th>
	<th><?php echo $paginator->sort('filename');?></th>
	<th><?php echo $paginator->sort('path');?></th>
	<th><?php echo $paginator->sort('mimetype');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($requests as $request):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $request['Request']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($request['User']['email_address'], array('controller'=> 'users', 'action'=>'view', $request['User']['id'])); ?>
		</td>
		<td>
			<?php echo inet_dtop($request['Request']['ip_address']); ?>
		</td>
		<td>
			<?php echo $html->link($request['Doctype']['name'], array('controller'=> 'doctypes', 'action'=>'view', $request['Doctype']['id'])); ?>
		</td>
		<td>
			<?php echo $request['Request']['filename']; ?>
		</td>
		<td>
			<?php echo $request['Request']['path']; ?>
		</td>
		<td>
			<?php echo $request['Request']['mimetype']; ?>
		</td>
		<td>
			<?php echo $request['Request']['created']; ?>
		</td>
		<td>
			<?php echo $request['Request']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $request['Request']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $request['Request']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $request['Request']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $request['Request']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Request', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Doctypes', true), array('controller'=> 'doctypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Doctype', true), array('controller'=> 'doctypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
