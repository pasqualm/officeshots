<div class="factories form">
<?php echo $form->create('Factory');?>
	<fieldset>
		<legend>
			<?php if ($this->action == 'add') {
				__('Add a new factory');
			} else {
				__('Edit this factory');
			}
			?>
		</legend>
		<?php
			echo $form->input('name');
			echo $form->input('hardware');
		?>
		<div class="input select">
			<label for="FactoryOperatingsystemId">Operatingsystem</label>
			<select name="data[Factory][operatingsystem_id]" id="FactoryOperatingsystemId">
			<?php foreach ($operatingsystems as $os): ?>
				<option
					value="<?php echo $os['Operatingsystem']['id'];?>"
					<?php if ($this->data && $os['Operatingsystem']['id'] == $this->data['Factory']['operatingsystem_id']) {echo 'selected="selected"';} ?>
				>
					<?php echo $os['Operatingsystem']['name']; ?>
					<?php echo $os['Operatingsystem']['version']; ?>
					<?php if ($os['Operatingsystem']['codename']): ?>
						(<?php echo $os['Operatingsystem']['codename']; ?>)
					<?php endif; ?>
					&nbsp;
				</option>
			<?php endforeach; ?>
			</select>
		</div>
	</fieldset>
	<?php echo $form->end('Submit');?>
</div>
