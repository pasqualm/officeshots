<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class CronShell extends Shell
{
	/**
	 * Override startup() so no welcome message is printed
	 */
	public function startup() {}

	/**
	 * Main function. Print help and exit.
	 */
	public 	function main()
	{
		$this->help();
	}

	/**
	 * Expire old requests
	 */
	public function expire()
	{
		App::import('Model', 'Request');
		$Request = new Request();

		if (!$Request->expireAll()) {
			$this->log('Cron: Could not expire old requests.');
		}
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Commandline interface to the Officeshots cron jobs');
		$this->hr();
		$this->out("Usage: cake cron <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\texpire\n\t\tSet STATE_EXPIRED on all requests whose expiry time has passed.\n\t\tThis should be run every minute.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
