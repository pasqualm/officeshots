<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Result model
 */
class Result extends AppModel
{
	/**#@+
	 * Request states
	 */
	const STATE_UPLOADING   = 1;
	const STATE_SCAN_QUEUED = 2;
	const STATE_SCAN_FOUND  = 4;
	const STATE_SCAN_FAILED = 8;
	const STATE_FINISHED    = 16;
	/**#@-*/
	
	/** @var array A result belongs to a job and a factory that created it */
	public $belongsTo = array('Factory', 'Format');

	/**
	 * @var array A result is always associated with a job.
	 *
	 * This looks backwards but otherwise it is impossible to find Jobs that have no Result yet.
	 */
	public $hasOne = array('Job');

	/** @var array The result is a file */
	public $actsAs = array('File' => 'files/results', 'Containable', 'BeanStalk.Deferrable');

	/** @var string Use the filename as the distinguising name */
	public $displayField = 'filename';

	/**
	 * When deleting a result, find the Job that has it's ID and clear it.
	 */
	public function beforeDelete()
	{
		$data = $this->read();
		$this->Job->id = $data['Job']['id'];
		$this->Job->saveField('result_id', '');
		return true;
	}

	/**
	 * After creating a new result, schedule it for scanning
	 */
	public function afterSave($created)
	{
		if ($created === false || empty($this->data['Result']['path'])) {
			return;
		}

		if (!$this->defer('scan')) {
			$this->log('Failed to queue the result for the virus scanner.', true);
		}

		$this->saveField('state', self::STATE_SCAN_QUEUED);
	}

	/**
	 * Scan the request for viruses and update the scan status
	 *
	 * @return None
	 */
	public function scan()
	{
		$this->read();

		// Check for the correct state. In case of queue errors a scan could be scheduled multiple times
		if ($this->data['Result']['state'] != self::STATE_SCAN_QUEUED) {
			$this->log('Result ID ' . $this->id . ' is not queued for scan.');
			return;
		}

		$clamd = new Clamd();
		$path = $this->getPath();
		$result = '';

		if (!$path) {
			$this->log('Result could not be scanned. ' . $path . ' (Result ID: ' . $this->id . ') does not exists. ');
			return;
		}

		$status = $clamd->scan($path, $result);
		if ($status == Clamd::OK) {
			// Scan OK. Queue the Request for processing
			$this->data['Result']['state'] = self::STATE_FINISHED;
			$this->log('Clamd scanned ' . $path . ' (Result ID: ' . $this->id . '): OK', LOG_DEBUG);
		} elseif ( $status == Clamd::FOUND ) {
			// Note that the file is *not* deleted. This was we can later check if there really was a virus
			$this->data['Result']['state'] = self::STATE_SCAN_FOUND;
			$this->data['Result']['state_info'] = $result;
			$this->log('Clamd scanned ' . $path . ' (Result ID: ' . $this->id . '): FOUND ' . $result, LOG_DEBUG);
		} else {
			// There was an error.
			if ($status === false) {
				$result = $clamd->lastError();
				$status = Clamd::ERROR;
			}

			$this->data['Result']['state'] = self::STATE_SCAN_FAILED;
			$this->data['Result']['state_info'] = $result;

			$this->log('Clamd error scanning ' . $path . ' (Result ID: ' . $this->id . '): ' . $result);
		}

		$this->save();
	}
}

?>
