<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Gallery model
 *
 * Galleries can be used to publicly display requests
 */
class Gallery extends AppModel
{
	/** @var array Galleries have an owner and optionally a group who can edit the gallery */
	public $belongsTo = array('Group', 'User');

	/** @var string Every application supports a certain ODF doctype */
	public $hasAndBelongsToMany = 'Request';

	/** @var array The model behaviors */
	public $actsAs = array(
		'Containable',
		'Sluggable' => array('label' => 'name', 'overwrite' => true)
	);

	/**
	 * Add a request to a gallery
	 * @param string $request_id The request to add
	 * @param string $id The gallery ID, or $this->id
	 */
	public function addRequest($request_id, $id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		$relation = $this->GalleriesRequest->find('first', array('conditions' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));

		if (!empty($relation)) {
			return true;
		}

		$this->GalleriesRequest->create();
		return $this->GalleriesRequest->save(array('GalleriesRequest' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));
	}

	/**
	 * Remove a request from a gallery
	 * @param string $request_id The request to add
	 * @param string $id The gallery ID, or $this->id
	 */
	public function removeRequest($request_id, $id = null)
	{
		if (!$id) {
			$id = $this->id;
		}

		$relation = $this->GalleriesRequest->find('first', array('conditions' => array(
			'gallery_id' => $id,
			'request_id' => $request_id,
		)));

		if (!empty($relation)) {
			return $this->GalleriesRequest->del($relation['GalleriesRequest']['id']);
		}

		return true; // The relation didn't exist to begin with
	}

	/**
	 * Convert the Markdown description to HTML before saving
	 * @return boolean True to continue saving
	 */
	public function beforeSave()
	{
		if (isset($this->data['Gallery']['description'])) {
			App::import('Vendor', 'markdown');
			App::import('Vendor', 'HTMLPurifier', array('file' => 'htmlpurifier/HTMLPurifier.standalone.php'));

			$config = HTMLPurifier_Config::createDefault();
			$config->set('Cache.SerializerPath', CACHE . DS . 'htmlpurifier');
			$purifier = new HTMLPurifier($config);

			$desc_html = Markdown($this->data['Gallery']['description']);
			$this->data['Gallery']['description_html'] = $purifier->purify($desc_html);
		}

		return true;
	}
}

?>
