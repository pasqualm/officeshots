set LOCALPYTHON=C:\Python26
set OOOPATH=C:\Program Files\OpenOffice.org 3

set PYTHONPATH=%LOCALPYTHON%\DLLs
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\plat-win
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\tk
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages\win32
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages\win32\lib
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages\win32com
set PYTHONPATH=%PYTHONPATH%;%LOCALPYTHON%\lib\site-packages\win32comext

cd ..\src
"%OOOPATH%\program\python.exe" factory.py %*
cd ..\utils
