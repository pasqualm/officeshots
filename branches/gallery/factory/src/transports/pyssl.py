# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module provides SSLTransport in order to have SSL client authentication
with the standard xmlrpclib. It is based on Python's own httplib.
"""

from xmlrpclib import Transport

class SSLTransport(Transport):
	"""
	An SSL-capable transport compatible with xmlrpclib. Note that this transport
	does not verify the server signature. Python's HTTPlib is not capable of that
	"""

	def __init__(self, key_file, cert_file):
		Transport.__init__(self)
		self.key_file = key_file
		self.cert_file = cert_file
	
	def make_connection(self, host):
		"""
		create a HTTPS connection object from a host descriptor
		host may be a string, or a (host, x509-dict) tuple
		"""
		import httplib
		host, extra_headers, x509 = self.get_host_info(host)
		try:
			HTTPS = httplib.HTTPS
		except AttributeError:
			raise NotImplementedError("your version of httplib doesn't support HTTPS")
		else:
			return apply(HTTPS, (host, None, self.key_file, self.cert_file), x509 or {})
