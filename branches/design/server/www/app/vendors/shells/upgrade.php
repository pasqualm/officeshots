<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Model', 'Request');

/**
 * A simple shell to upgrade the database
 */
class UpgradeShell extends Shell
{
	/** @var array The models to use */
	public $uses = array('Request', 'Result', 'User', 'Group');

	/**
	 * Main function. Print help and exit.
	 */
	public function main()
	{
		$this->help();
	}

	/**
	 * Try to determine the correct request state on all requests.
	 * This overrides any currently existing state!
	 */
	public function requestState()
	{
		$result = $this->in('This will override any existing state. Before you do this you need to have set the counterCaches! Continue?', array('y', 'n'));

		if ($result == 'n') {
			$this->_stop();
		}

		$requests = $this->Request->find('all');
		foreach ($requests as $request) {
			$request['Request']['state'] = Request::STATE_QUEUED;

			if (strtotime($request['Request']['expire']) <= time()) {
				$request['Request']['state'] = Request::STATE_EXPIRED;
			}

			if ($request['Request']['job_count'] > 0 && $request['Request']['job_count'] == $request['Request']['result_count']) {
				$request['Request']['state'] = Request::STATE_FINISHED;
			}

			if ($request['Request']['job_count'] == 0) {
				$request['Request']['state'] = Request::STATE_CANCELLED;
			}

			$this->Request->save($request);
		}
	}

	/**
	 * Upgrade from the sequential state to binary state flags
	 */
	public function requestStateFlag()
	{
		$request = $this->Request->find('first', array('order' => array('Request.state DESC')));

		if ($request['Request']['state'] > 7) {
			$this->out('It looks like your states have already have been upgraded.');
			$this->_stop();
		}

		// Sorted backwards
		$states = array(
			7 => 128,
			6 => 64,
			5 => 32,
			4 => 16,
			3 => 8,
			2 => 4,
			1 => 2,
			0 => 1,
		);

		foreach ($states as $state_old => $state_new) {
			$this->Request->updateAll(
				array('Request.state' => $state_new),
				array('Request.state' => $state_old)
			);
		}
	}

	/**
	 * Update the job and result counterCache on all requests.
	 */
	public function jobCount()
	{
		$requests = $this->Request->find('all');
		foreach ($requests as $request) {
			if (!isset($request['Job'][0]) || !is_array($request['Job'][0])) {
				continue;
			}

			$this->Request->Job->id = $request['Job'][0]['id'];
			$this->Request->Job->read();
			$this->Request->Job->updateCounterCache();
		}
	}

	/**
	 * Upgrade the result state
	 */
	public function resultState()
	{
		$this->Result->updateAll(
			array('state' => Result::STATE_FINISHED)
		);
	}

	/**
	 * Set the default group membership based on e-mail address lists
	 */
	public function defaultGroupMembership()
	{
		$users = $this->User->find('all', array('recursive' => -1));
		foreach($users as $user) {
			$this->Group->add_default_member($user['User']['id']);
		}
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Commandline interface to upgrade the database');
		$this->hr();
		$this->out("Usage: cake upgrade <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\trequestState\n\t\tTry to determine the correct state for all requests.");
		$this->out("\n\trequestStateFlag\n\t\tUpgrade from sequential to binary state flags.");
		$this->out("\n\tjobCount\n\t\tUpdate the job_count and result_count on all requests.");
		$this->out("\n\tresultState\n\t\tUpdate the result state to finished.");
		$this->out("\n\tdefaultGroupMembership\n\t\tUpdate the default group membership for all existing users.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
