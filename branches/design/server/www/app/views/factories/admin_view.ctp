<div class="factories view">
<h2><?php  __('Factory');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factory['User']['email_address'], array('controller'=> 'users', 'action'=>'view', $factory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Operatingsystem'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factory['Operatingsystem']['name'], array('controller'=> 'operatingsystems', 'action'=>'view', $factory['Operatingsystem']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hardware'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['hardware']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Poll'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['last_poll']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factory['Factory']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Factory', true), array('action'=>'edit', $factory['Factory']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Factory', true), array('action'=>'delete', $factory['Factory']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $factory['Factory']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Factories', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Factory', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Operatingsystems', true), array('controller'=> 'operatingsystems', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Operatingsystem', true), array('controller'=> 'operatingsystems', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Workers', true), array('controller'=> 'workers', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Worker', true), array('controller'=> 'workers', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Results', true), array('controller'=> 'results', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Result', true), array('controller'=> 'results', 'action'=>'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Workers');?></h3>
	<?php if (!empty($factory['Worker'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Factory Id'); ?></th>
		<th><?php __('Application Id'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($factory['Worker'] as $worker):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $worker['id'];?></td>
			<td><?php echo $worker['factory_id'];?></td>
			<td><?php echo $worker['application_id'];?></td>
			<td><?php echo $worker['version'];?></td>
			<td><?php echo $worker['created'];?></td>
			<td><?php echo $worker['modified'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'workers', 'action'=>'view', $worker['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'workers', 'action'=>'edit', $worker['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'workers', 'action'=>'delete', $worker['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $worker['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Worker', true), array('controller'=> 'workers', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Jobs');?></h3>
	<?php if (!empty($factory['Job'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Request Id'); ?></th>
		<th><?php __('Platform Id'); ?></th>
		<th><?php __('Application Id'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Result Id'); ?></th>
		<th><?php __('Factory Id'); ?></th>
		<th><?php __('Locked'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($factory['Job'] as $job):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $job['id'];?></td>
			<td><?php echo $job['request_id'];?></td>
			<td><?php echo $job['platform_id'];?></td>
			<td><?php echo $job['application_id'];?></td>
			<td><?php echo $job['version'];?></td>
			<td><?php echo $job['result_id'];?></td>
			<td><?php echo $job['factory_id'];?></td>
			<td><?php echo $job['locked'];?></td>
			<td><?php echo $job['created'];?></td>
			<td><?php echo $job['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'jobs', 'action'=>'view', $job['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'jobs', 'action'=>'edit', $job['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'jobs', 'action'=>'delete', $job['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Results');?></h3>
	<?php if (!empty($factory['Result'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Factory Id'); ?></th>
		<th><?php __('Format Id'); ?></th>
		<th><?php __('Filename'); ?></th>
		<th><?php __('Path'); ?></th>
		<th><?php __('Mimetype Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($factory['Result'] as $result):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $result['id'];?></td>
			<td><?php echo $result['factory_id'];?></td>
			<td><?php echo $result['format_id'];?></td>
			<td><?php echo $result['filename'];?></td>
			<td><?php echo $result['path'];?></td>
			<td><?php echo $result['mimetype_id'];?></td>
			<td><?php echo $result['created'];?></td>
			<td><?php echo $result['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'results', 'action'=>'view', $result['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'results', 'action'=>'edit', $result['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'results', 'action'=>'delete', $result['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $result['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Result', true), array('controller'=> 'results', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
