<div class="factories index">
<h2><?php __('Factories');?></h2>
<div class="actions">
	<ul>
		<?php if ($this->action == 'index'): ?>
			<li><?php __('View all'); ?></li>
			<li><?php echo $html->link(__('View active', true), array('action'=>'active')); ?> </li>
		<?php else: ?>
			<li><?php echo $html->link(__('View all', true), array('action'=>'index')); ?> </li>
			<li><?php __('View active'); ?></li>
		<?php endif; ?>
		<?php if ($canAddFactories): ?>
			<li><?php echo $html->link(__('Add another factory', true), array('action'=>'add')); ?> </li>
		<?php endif; ?>
	</ul>
</div>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php __('Owner');?></th>
	<th><?php __('Operatin system');?></th>
	<th><?php __('Hardware');?></th>
	<th><?php echo $paginator->sort('Last activity', 'last_poll');?></th>
</tr>
<?php
$i = 0;
foreach ($factories as $factory):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $html->link($factory['Factory']['name'], array('action' => 'view', $factory['Factory']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($factory['User']['name'], array('controller'=> 'users', 'action'=>'view', $factory['User']['id'])); ?>
		</td>
		<td>
			<?php echo $factory['Operatingsystem']['name']; ?>
			<?php echo $factory['Operatingsystem']['version']; ?>
			<?php if ($factory['Operatingsystem']['codename']): ?>
				(<?php echo $factory['Operatingsystem']['codename']; ?>)
			<?php endif; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['hardware']; ?>
		</td>
		<td>
			<?php echo $factory['Factory']['last_poll']; ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
