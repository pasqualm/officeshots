<div class="users form">
<p><?php __('Enter your e-mail address and we will send you an e-mail containing instructions to reset your password.');?></p>

<?php echo $form->create('User', array('action' => 'recover'));?>
	<fieldset>
 		<legend><?php __('Recover your account password');?></legend>
		<?php
			echo $form->input('email_address', array('value' => ''));
		?>
	</fieldset>
<?php echo $form->end('Submit');?>

</div>
