<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The platforms controller
 */
class PlatformsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');

	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	public function admin_index()
	{
		$this->Platform->recursive = 0;
		$this->set('platforms', $this->paginate());
	}

	public function admin_view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Platform.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('platform', $this->Platform->read(null, $id));
	}

	public function admin_add()
	{
		if (!empty($this->data)) {
			$this->Platform->create();
			if ($this->Platform->save($this->data)) {
				$this->Session->setFlash(__('The Platform has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Platform could not be saved. Please, try again.', true));
			}
		}
	}

	public function admin_edit($id = null)
	{
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Platform', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Platform->save($this->data)) {
				$this->Session->setFlash(__('The Platform has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Platform could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Platform->read(null, $id);
		}
	}

	public function admin_reset()
	{
		$this->Platform->resetweights();
		$this->redirect(array('action' => 'index'));
	}

	public function admin_moveup($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Platform.', true));
		} else {
			$this->Platform->moveup($id);
		}

		$this->redirect(array('action' => 'index'));
	}

	public function admin_movedown($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Platform.', true));
		} else {
			$this->Platform->movedown($id);
		}

		$this->redirect(array('action' => 'index'));
	}

	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Platform', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Platform->del($id)) {
			$this->Session->setFlash(__('Platform deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}
}

?>
