<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Core', 'clamd.Clamd');

/**
 * A simple shell to interface with Clamd
 */
class ClamdShell extends Shell
{
	/** @var object Reference to the Clamd object */
	private $clamd = null;

	/**
	 * Print a welcome message
	 */
	public function initialize() {
		$this->clamd = new Clamd();
		$this->out('Cake Clamd Shell. Type "help" for usage information.');
		$this->hr();
	}

	/**
	 * Main shell execution loop
	 */
	public function main()
	{
		while (true) {
			$text = $this->in('');

			if ($text == 'quit' || $text == 'exit' || $text == 'q') {
				$this->_stop();
			}

			if ($text == 'ping') {
				if (!$response = $this->clamd->ping()) {
					$this->out('Error: ' . $this->clamd->lastError());
				} else {
					$this->out('PONG');
				}

				continue;
			}

			if (substr($text, 0, 4) == 'scan') {
				$msg = '';
				$file = substr($text, 5);
				$this->clamd->clearLastError();
				$response = $this->clamd->scan($file, $msg);

				if ($response === false) {
					$this->out('Error: ' . $this->clamd->lastError());
				} elseif ($response == Clamd::OK) {
					$this->out($file . ' is clean');
				} elseif ($response == Clamd::FOUND) {
					$this->out($file . ' is infected with ' . $msg);
				} else {
					$this->out('Error: ' . $msg);
				}
				continue;
			}

			if (substr($text, 0, 5) == 'rscan') {
				$file = substr($text, 6);
				$this->clamd->clearLastError();
				if (!$response = $this->clamd->rscan($file, true)) {
					$this->out('Error: ' . $this->clamd->lastError());
					continue;
				}

				foreach ($response as $file) {
					if ($file['status'] == Clamd::FOUND) {
						$this->out($file['file'] . ' is infected with ' . $file['msg']);
					} else {
						$this->out($file['file'] . ' error: ' . $file['msg']);
					}
				}
				continue;
			}

			if (substr($text, 0, 4) == 'help') {
				$this->help();
				continue;
			}

			$this->out('Unknown command: ' . $text);
		}
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Interactive commandline interface to the Clamd plugin');
		$this->hr();
		$this->out("Usage: cake clamd");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\texit|quit|q\n\t\tQuit the shell");
		$this->out("\n\tping\n\t\tSend a PING command to the clamav daemon");
		$this->out("\n\tscan <file>\n\t\tScan <file> for viruses");
		$this->out("\n\trscan <directory>\n\t\tRecursively scan <directory> for viruses. Only infected files and errors are returned.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
