<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// We need to access the Request model statically for it's state constants
App::import('Model', 'Request');

/**
 * The Job model
 */
class Job extends AppModel
{
	/**#@+
	 * Job model states
	 */
	const STATE_QUEUED   = 1;
	const STATE_FINISHED = 2;
	const STATE_FAILED   = 4;
	/**#@-*/

	/**
	 * @var array All jobs belong to a request and factory and are associated with a certain application and platform
	 *
	 * The Result seems backwards but we need the foreign key in the jobs table otherwise it's not possible to
	 * search for jobs that have no result yet.
	 */
	public $belongsTo = array('Request' => array('counterCache' => true),
	                          'Platform', 'Application', 'Factory', 'Result', 'Format');

	/** @var array Job queries can be quite complex, so use Containable */
	public $actsAs = array('Containable');

        /**
         * Delete any existing result and re-schedule the job for a fresh rendering
         */
        public function rerun()
        {
                if ($result_id = $this->field('result_id')) {
                        $this->Result->delete($result_id);
                }

                $this->save(array(
                        'factory_id' => '',
                        'state'      => self::STATE_QUEUED,
                        'failures'   => 0,
                        'locked'     => '0000-00-00 00:00:00',
                ));
        }

	/**
	 * Manually update the counterCaches for Request
	 *
	 * We use a manual counterCache because we need two counts on Request. CakePHP's counterCache can only take one.
	 *
	 * @param array $keys The Job model data
	 * @param boolean $created Ignored, like in the Model class
	 */
	public function updateCounterCache($keys = array(), $created = false)
	{
		$request_id   = $this->field('request_id');
		$job_count    = $this->find('count', array('conditions' => array('request_id' => $request_id)));
		$result_count = $this->find('count', array('conditions' => array('request_id' => $request_id, 'result_id <>' => '')));

		$result = $this->Request->updateAll(
			array(
				'Request.job_count' => $job_count,
				'Request.result_count' => $result_count
			),
			array('Request.id' => $request_id)
		);

		// If this was the last job, change Request state to finished
		$this->Request->id = $request_id;
		if ($this->Request->field('state') == Request::STATE_QUEUED && $job_count == $result_count && $job_count > 0) {
			$this->Request->saveField('state', Request::STATE_FINISHED);
		}
	}

	/**
	 * Remove the result before removing the Job
	 */
	public function beforeDelete($cascade)
	{
		$result_id = $this->field('result_id');

		if ($result_id) {
			$this->Result->delete($result_id);
		}

		return true;
	}
}

?>
