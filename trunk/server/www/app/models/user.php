<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The basic User model
 */
class User extends AppModel
{
	/** @var string The primary field */
	public $displayField = 'email_address';

	/** @var order by e-mail address */
	public $order = array('User.email_address' => 'asc');

	/** @var array The validation model */
	public $validate = array(
		'email_address' => array('email'),
		'password' => array('alphaNumeric'),
		'active' => array('numeric')
	);

	/** @var array Users can have factories */
	public $hasMany = array('Factory');

	/** @var array Many-to-many relationships */
	public $hasAndBelongsToMany = array(
		'Group' => array('unique' => true)
	);

	/** @var array Use Containable */
	public $actsAs = array('Containable');

	/**
	 * Get the sha1 hash of a certain field
	 */
	private function getHash($field)
	{
		if (!$this->id) {
			return false;
		}

		return Security::hash($this->field($field), 'sha1', true);
	}

	/**
	 * Get the user activation hash
	 */
	public function getActivationHash()
	{
		return $this->getHash('created');
	}

	/**
	 * Get the user password recovery hash
	 */
	public function getRecoveryHash()
	{
		return $this->getHash('password');
	}

	/**
	 * Attempt to activate the user based on the supplied has value
	 *
	 * @param string $hash the hash to verify against
	 * @return boolean
	 */
	public function activate($hash)
	{
		if (!$this->id || !$hash) {
			return false;
		}

		if ($hash === $this->getActivationHash()) {
			$this->saveField('active', true);
			return true;
		}

		return false;
	}

	/**
	 * After creating a new user, add them to all the default groups
	 * @param boolean $created Whether the saved user was newly created or not
	 */
	public function afterSave($created)
	{
		if (!$created) {
			return;
		}

		$groups = $this->Group->find('all', array(
			'conditions' => array('Group.default' => 1),
		));

		foreach ($groups as $group) {
			$this->Group->add_member($this->id, $group['Group']['id']);
		}
	}
}

?>
