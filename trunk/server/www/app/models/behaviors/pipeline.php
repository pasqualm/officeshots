<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The PinelineBehavior can be used to implement generic pre- and
 * postprocessor pipelines
 */
class PipelineBehavior extends ModelBehavior
{
	/** @var array The initial configuration */
	public $config = array();

	/**
	 * Setup the behavior.
	 * @var Model &$Model A reference to the model
	 * @var array $config The initial configuration
	 */
	public function setup(&$Model, $config = array())
	{
		if (!$config) {
			return;
		}

		if (!is_array($config)) {
			$config = array($config);
		}

		$this->config = $config;

		foreach ($config as $pipeline) {
			$Model->{$pipeline} = new ModelPipeline($Model);
		}
	}

	/**
	 * Run one or more pipelines
	 * @var Model &$Model A reference to the model
	 * @param array $pipelines The pipelines to run. Leave empty to run all pipelines.
	 */
	public function run(&$Model, $pipelines = array())
	{
		if (empty($pipelines)) {
			$pipelines = $this->config;
		}

		if (!is_array($pipelines)) {
			$pipelines = array($pipelines);
		}

		foreach ($pipelines as $pipeline) {
			$Model->{$pipeline}->run();
		}
	}
}

/**
 * A pipeline object
 */
class ModelPipeline extends Object
{
	/** @var Model A reference to the model */
	private $Model = null;

	/** @var array A list of callbacks to call */
	private $callbacks = array();

	/** @var array A list of errbacks to call when one of teh callbacks fails */
	private $errbacks = array();

	/**
	 * The constructor
	 */
	public function __construct($Model)
	{
		$this->Model = $Model;
	}

	/**
	 * Add a callback
	 * @param string $callback The model method name of the callback
	 * @param ... Any arguments that need to be passed to the callback
	 * @return boolean Success
	 */
	public function callback($callback)
	{
		$args = func_get_args();
		$_ = array_shift($args); // Drop the callback

		return $this->add($callback, $args, 'callbacks');
	}

	/**
	 * Add an errback
	 * @param string $errback The model method name of the errback
	 * @param ... Any arguments that need to be passed to the errback
	 * @return boolean Success
	 */
	public function errback($errback)
	{
		$args = func_get_args();
		$_ = array_shift($args); // Drop the errback

		return $this->add($errback, $args, 'errbacks');
	}

	/**
	 * Add a callback or errback
	 * @param string $function The model method name
	 * @param array $args Any arguments that need to be passed to the function
	 * @param string $type The function type (callback or errback)
	 * @return boolean Success
	 */
	private function add($function, $args = array(), $type = 'callbacks')
	{
		if (!method_exists($this->Model, $function)) {
			return false;
		}

		array_push($this->{$type}, array(
			'function' => $function,
			'args' => $args
		));

		return true;
	}

	/**
	 * Run the pipeline
	 * This calls all the callbacks in order until one returns false or until the end
	 * When one callback returns false, the errbacks are called.
	 */
	public function run()
	{
		foreach ($this->callbacks as $callback) {
			if (!call_user_func_array(array($this->Model, $callback['function']), $callback['args'])) {
				foreach ($this->errbacks as $errback) {
					call_user_func_array(array($this->Model, $errback['function']), $errback['args']);
				}
				return;
			}
		}
	}
}

?>
