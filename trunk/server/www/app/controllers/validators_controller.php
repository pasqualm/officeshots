<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Validators controller
 */
class ValidatorsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'ValidatorModel');
	
	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/**
	 * Set the auth permissions for this controller
	 * @return void
	 */
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->AuthCert->allow('view');
	}

	/**
	 * Get the result by it's ID and check access control.
	 *
	 * Calling methods should ensure that Job.Request is contained in the model query.
	 *
	 * @param string $id The result ID
	 * @param return array An array containing the result.
	 */
	private function _getValidator($id)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Result.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'add'));
		}

		$this->Validator->contain(array(
			'Request',
			'Result',
			'Result.Job',
			'Result.Job.Application',
			'Result.Job.Platform',
			'Result.Job.Request',
		));
		
		$validator = $this->Validator->read(null, $id);
		if (isset($validator['Result']['Job']['Request'])) {
			$validator['Request'] = $validator['Result']['Job']['Request'];
		}

		if ($this->Validator->Request->checkAccess($this->AuthCert->user('id'), 'read', $validator['Request']['id'])) {
			return $validator;
		}

		$this->Session->setFlash(__('Invalid validator.', true));
		$this->redirect(array('controller' => 'requests', 'action'=>'add'));
	}
	
	/**
	 * You can't index results. Redirect to requests.
	 * @return void
	 */
	public function index()
	{
		$this->redirect(array('controller' => 'requests', 'action' => 'index'));
	}

	/**
	 * View a single Validator result
	 *
	 * @param string $id The Validator ID
	 * @return void
	 */
	public function view($id = null)
	{
		$validator = $this->_getValidator($id);

		// Strip HTML skeleton
		if (preg_match('#^<html>.*<body>(.*)</body>#si', $validator['Validator']['response'], $match)) {
			$validator['Validator']['response'] = $match[1];
		}

		$this->set(array(
			'validator' => $validator,
		));
	}
}

?>
