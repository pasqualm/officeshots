<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The applications controller
 */
class ApplicationsController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/** @var array The helpers this controller uses */
	public $helpers = array('Html', 'Form');

	/**
	 * Make a list of all images
	 */
	private function _getImages()
	{
		$images = glob(WWW_ROOT . 'img/icons/applications/*.*');
		foreach ($images as &$image) {
			$image = basename($image);
		}

		return array_combine($images, $images);
	}

	/**
	 * Show the index of all applications
	 */
	public function admin_index() {
		$this->Application->recursive = 0;
		$this->set('applications', $this->paginate());
	}

	/**
	 * View a single application
	 * @param string $id The application ID
	 */
	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Application.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('application', $this->Application->read(null, $id));
	}

	/**
	 * Add a new application
	 */
	public function admin_add() {
		if (!empty($this->data)) {
			$this->Application->create();
			if ($this->Application->save($this->data)) {
				$this->Session->setFlash(__('The Application has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Application could not be saved. Please, try again.', true));
			}
		}

		$doctypes = $this->Application->Doctype->find('list');
		$images   = $this->_getImages();

		$this->set(compact('doctypes', 'images'));
		$this->render('admin_edit');
	}

	/**
	 * Edit an applications
	 * @param string $id The application ID
	 */
	public function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Application', true));
			$this->redirect(array('action'=>'index'));
		}

		if (!empty($this->data)) {
			if ($this->Application->save($this->data)) {
				$this->Session->setFlash(__('The Application has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Application could not be saved. Please, try again.', true));
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Application->read(null, $id);
		}

		$doctypes = $this->Application->Doctype->find('list');
		$images   = $this->_getImages();

		$this->set(compact('doctypes', 'images'));
	}

	/**
	 * Delete an application
	 * @param string $id The application ID
	 */
	public function admin_delete($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid id for Application', true));
			$this->redirect(array('action'=>'index'));
		}

		if (!empty($this->data)) {
			// Migrate application
			$success = (
				$this->Application->migrate($this->data['Application']['id'], $this->data['Application']['new_id'])
				&& $this->Application->del($id)
			);
	       		
			if ($success) {
				$this->Session->setFlash(__('Application deleted', true));
				$this->redirect(array('action'=>'index'));
			}
		}

		if (empty($this->data)) {
			$this->data = $this->Application->read(null, $id);
		}


		$applications = $this->Application->find('list');
		if (isset($applications[$id])) {
			unset($applications[$id]);
		}

		$this->set(compact('applications'));
	}

}
?>
