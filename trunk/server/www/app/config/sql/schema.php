<?php 
/* SVN FILE: $Id$ */
/* App schema generated on: 2010-06-25 14:06:50 : 1277467250*/
class AppSchema extends CakeSchema {
	var $name = 'App';

	function before($event = array()) {
		return true;
	}

	function after($event = array()) {
	}

	var $applications = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'icon' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $applications_doctypes = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'application_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'doctype_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'application_id' => array('column' => 'application_id', 'unique' => 0), 'doctype_id' => array('column' => 'doctype_id', 'unique' => 0))
		);
	var $doctypes = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'code' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 3),
			'order' => array('type' => 'integer', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $doctypes_workers = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'doctype_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'worker_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'doctype_id' => array('column' => 'doctype_id', 'unique' => 0), 'worker_id' => array('column' => 'worker_id', 'unique' => 0))
		);
	var $factories = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'user_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'operatingsystem_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'hardware' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'last_poll' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
			'active_since' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $formats = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'icon' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'code' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 3),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $formats_workers = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'format_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'worker_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $galleries = array(
			'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
			'parent_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
			'lft' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
			'rght' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'slug' => array('type' => 'string', 'null' => false, 'default' => NULL, 'key' => 'index'),
			'user_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'group_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'description' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'description_html' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'slug' => array('column' => 'slug', 'unique' => 0), 'parent_id' => array('column' => 'parent_id', 'unique' => 0), 'lft' => array('column' => 'lft', 'unique' => 0), 'rght' => array('column' => 'rght', 'unique' => 0))
		);
	var $galleries_requests = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'gallery_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
			'request_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'gallery_id' => array('column' => 'gallery_id', 'unique' => 0), 'request_id' => array('column' => 'request_id', 'unique' => 0))
		);
	var $groups = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'request_limit' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 5),
			'default' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
			'default_memberlist' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $groups_users = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'group_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'user_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $jobs = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'request_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'platform_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'application_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'version' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 32),
			'format_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'result_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'factory_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'state' => array('type' => 'integer', 'null' => false, 'default' => '1'),
			'failures' => array('type' => 'integer', 'null' => false, 'default' => '0'),
			'locked' => array('type' => 'datetime', 'null' => false, 'default' => '0000-00-00 00:00:00'),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'updated' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $mimetypes = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'key' => 'index'),
			'icon' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'extension' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 4),
			'doctype_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'format_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'mimetype' => array('column' => 'name', 'unique' => 0), 'doctype_id' => array('column' => 'doctype_id', 'unique' => 0), 'format_id' => array('column' => 'format_id', 'unique' => 0))
		);
	var $operatingsystems = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'platform_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'version' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20),
			'codename' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $permissions = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'group_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $platforms = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'order' => array('type' => 'integer', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $requests = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'user_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'ip_address' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => 39, 'key' => 'index'),
			'filename' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'path' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'root' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'mimetype_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'page_start' => array('type' => 'integer', 'null' => false, 'default' => '1'),
			'page_end' => array('type' => 'integer', 'null' => false, 'default' => '0'),
			'priority' => array('type' => 'integer', 'null' => false, 'default' => '2'),
			'own_factory' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
			'state' => array('type' => 'integer', 'null' => false, 'default' => '1'),
			'state_info' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'description' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'description_html' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'job_count' => array('type' => 'integer', 'null' => false, 'default' => '0'),
			'result_count' => array('type' => 'integer', 'null' => false, 'default' => '0'),
			'expire' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'ip_address' => array('column' => 'ip_address', 'unique' => 0))
		);
	var $results = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'factory_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'format_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'filename' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'path' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'mimetype_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'state' => array('type' => 'integer', 'null' => false, 'default' => '1'),
			'state_info' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'verified' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 4),
			'description' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'description_html' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'updated' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $testsuites = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'source' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'root' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'gallery_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'gallery_id' => array('column' => 'gallery_id', 'unique' => 0))
		);
	var $users = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 127),
			'email_address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 127),
			'password' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 40),
			'active' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
	var $validators = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'parent_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'index'),
			'name' => array('type' => 'string', 'null' => false, 'default' => NULL),
			'state' => array('type' => 'integer', 'null' => false, 'default' => '1'),
			'response' => array('type' => 'text', 'null' => false, 'default' => NULL),
			'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'parent_id' => array('column' => 'parent_id', 'unique' => 0))
		);
	var $workers = array(
			'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary'),
			'factory_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'application_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36),
			'version' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 32),
			'development' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
			'testsuite' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
			'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
			'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1))
		);
}
?>