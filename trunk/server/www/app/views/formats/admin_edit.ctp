<div class="formats form">
<?php echo $form->create('Format');?>
	<fieldset>
		<legend><?php
			if ($this->action == 'admin_edit') {
				__('Edit Format');
			} else {
				__('Add Format');
			}
		?></legend>
	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('icon', array('type' => 'select', 'options' => $icons, 'empty' => true));
		echo $form->input('code');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
