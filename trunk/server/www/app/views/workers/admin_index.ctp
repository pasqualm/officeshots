<div class="workers index">
<h2><?php __('Workers');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('factory_id');?></th>
	<th><?php echo $paginator->sort('application_id');?></th>
	<th><?php echo $paginator->sort('version');?></th>
	<th><?php echo $paginator->sort('development');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($workers as $worker):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $html->link($worker['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $worker['Factory']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($worker['Application']['name'], array('controller'=> 'applications', 'action'=>'view', $worker['Application']['id'])); ?>
		</td>
		<td>
			<?php echo $worker['Worker']['version']; ?>
		</td>
		<td>
			<?php if ($worker['Worker']['development']) { __('Unstable'); } else { __('Stable'); } ?>
		</td>
		<td>
			<?php echo $worker['Worker']['created']; ?>
		</td>
		<td>
			<?php echo $worker['Worker']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $worker['Worker']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $worker['Worker']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $worker['Worker']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
