<script type="text/javascript">
	$(function() {
		doctypes_map = <?php echo $javascript->object($applicationDoctypes);?>;

		select_doctypes = function() {
			app = $("#WorkerApplicationId").val();
			doctypes = doctypes_map[app];

			$('#workerDoctypes input[type=checkbox]').each(function(i) {
				if (jQuery.inArray(this.value, doctypes) > -1) {
					$(this).removeAttr('disabled');
					$('label[for=' + this.id + ']').toggleClass('disabled', false);
				} else {
					$(this).removeAttr('checked').attr('disabled', 'disabled');
					$('label[for=' + this.id + ']').toggleClass('disabled', true);
				}
			});
		}

		$("#WorkerApplicationId").change(function() {
			select_doctypes();
		});

		select_doctypes();
	})
</script>

<?php
	$javascript->link('jquery-1.3.2.min.js', false);
	$javascript->link('jquery.rich-array-min.js', false);
?>

<div class="workers form">
<?php echo $form->create('Worker');?>
	<fieldset>
		<legend>
			<?php 
				if ($this->action == 'add' || $this->action == 'admin_add') {
					printf(__('Add an application to factory "%s"', true), $this->data['Factory']['name']);
				} else {
					printf(__('Edit this application on factory "%s"', true), $this->data['Factory']['name']);
				}
			?>
		</legend>
	<?php
		if ($this->action == 'add' || $this->action == 'admin_add') {
			echo $form->hidden('factory_id');
		}
		echo $form->hidden('Factory.name');
		echo $form->input('application_id');
		echo $form->input('version');
		echo $form->input('development', array('label' => __('This is an unstable development version, beta or release candidate', true)));
		echo $form->input('testsuite', array('label' => __('Participate in rendering the ODF test suites (Note: This can be hundreds of jobs)', true)));
		echo $form->input('Doctype', array('label' => __('Supported document types', true), 'type' => 'select', 'multiple' => 'checkbox', 'div' => array('id' => 'workerDoctypes')));
		echo $form->input('Format', array('label' => __('Supported output formats', true), 'type' => 'select', 'multiple' => 'checkbox'));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>

</div>
