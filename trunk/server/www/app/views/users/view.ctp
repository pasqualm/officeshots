<div class="users view">
	<h2>
		<?php if ($isSelf) {
			__('Your account');
		} else {
			echo $user['User']['name'];
		}
		?>
	</h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<?php if ($isSelf || $this->action == 'admin_view'): ?>
			<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
			<dd<?php if ($i++ % 2 == 0) echo $class;?>>
				<?php echo $user['User']['name']; ?>
				&nbsp;
			</dd>
			<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email Address'); ?></dt>
			<dd<?php if ($i++ % 2 == 0) echo $class;?>>
				<?php echo $user['User']['email_address']; ?>
				&nbsp;
			</dd>
		<?php endif; ?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Registered since'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['created']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php if ($isSelf || $this->action == 'admin_view'): ?>
	<p><?php echo $html->link(__('Edit account information', true), array('action'=>'edit', $user['User']['id'])); ?> </p>
	<div class="related">
		<h3>
			<?php if ($isSelf) {
				__('You are a member of the following groups');
			} else {
				printf(__('%s is a member of the following groups', true), $user['User']['name']);
			}
			?>
		</h3>
		<?php if (!empty($user['Group'])):?>
		<ul>
			<?php foreach ($user['Group'] as $group): ?>
			<li>
				<?php if ($this->action = 'admin_view') {
					echo $html->link($group['name'], array('controller' => 'groups', 'action' => 'view', $group['id']));
				} else { 
					echo $group['name'];
				}
				?>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</div>
<?php endif; ?>

<?php if ($canAddFactories || !empty($user['Factory'])): ?>
<div class="related">
	<h3>
		<?php
			if ($isSelf) {
				__('You own the following factories');
			} else {
				printf(__('%s owns the following factories', true), $user['User']['name']);
			}
		?>
	</h3>
	<ul>
		<?php if (empty($user['Factory'])): ?>
			<li><?php __('None'); ?></li>
		<?php else: ?>
			<?php foreach ($user['Factory'] as $factory): ?>
			<li><?php echo $html->link($factory['name'], array('controller' => 'factories', 'admin' => false, 'action' => 'view', $factory['id']));?></li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
	<?php if ($canAddFactories) {
		echo '<p>' . $html->link(__('Add another factory', true), array('controller' => 'factories', 'action'=>'add')) . '</p>';
	}?>
</div>
<?php endif; ?>
