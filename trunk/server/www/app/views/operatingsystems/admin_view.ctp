<div class="operatingsystems view">
<h2><?php  __('Operatingsystem');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $operatingsystem['Operatingsystem']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Version'); ?></dt>
		<dd>
			<?php echo $operatingsystem['Operatingsystem']['version']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Codename'); ?></dt>
		<dd>
			<?php echo $operatingsystem['Operatingsystem']['codename']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Platform'); ?></dt>
		<dd>
			<?php echo $html->link($operatingsystem['Platform']['name'], array('controller'=> 'platforms', 'action'=>'view', $operatingsystem['Platform']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php __('Created'); ?></dt>
		<dd>
			<?php echo $operatingsystem['Operatingsystem']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Modified'); ?></dt>
		<dd>
			<?php echo $operatingsystem['Operatingsystem']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Operatingsystem', true), array('action'=>'edit', $operatingsystem['Operatingsystem']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Operatingsystem', true), array('action'=>'delete', $operatingsystem['Operatingsystem']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $operatingsystem['Operatingsystem']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Operatingsystems', true), array('action'=>'index')); ?> </li>
	</ul>
</div>
