<?php foreach ($gallery['children'] as $child):?>
	<tr class="subgallery">
		<td class="subgallery-name" style="padding-left: <?php echo $indent;?>em"><?php echo $html->link($child['Gallery']['name'] . '/', array('action' => 'view', $child['Gallery']['slug']));?></td>
		<td>&nbsp;</td>
		<?php if ($abbreviate): ?>
			<td>-</td>
		<?php endif; ?>
		<td>-</td>
		<td>-</td>
		<?php if ($access):?><td>&nbsp;</td><?php endif;?>
	</tr>
	<?php echo $this->element('gallery', array('gallery' => $child, 'indent' => $indent + 2)); ?>
<?php endforeach;?>

<?php if (isset($gallery['Request']) && is_array($gallery['Request'])):?>
<?php foreach ($gallery['Request'] as $request):?>
	<tr class="request">
		<td class="request-name" style="padding-left: <?php echo $indent;?>em">
			<?php echo $html->link($request['filename'], array('controller'=> 'requests', 'action'=>'view', $request['id'])); ?>
		</td>
		<td><?php foreach ($request['Validator'] as $validator) { echo $validatorModel->getStateIcon($validator) . ' '; } ?></td>
		<?php if ($abbreviate): ?>
			<td>
				<?php echo $request['result_count'] . '/' . $request['job_count']; ?>
			</td>
		<?php endif; ?>
		<td><?php echo $request['created'];?></td>
		<td><?php echo $requestModel->getState($request); ?></td>
		<?php if ($access):?>
			<td class="actions">
				<?php echo $html->link(__('Remove', true), array('action' => 'remove_document', $gallery['Gallery']['slug'], $request['id'])); ?>
			</td>
		<?php endif;?>
	</tr>
	<?php if (!$abbreviate): ?>
		<?php foreach ($request['Job'] as $job):?>
			<tr class="result<?php if (!$job['Result'] || !$job['Result']['id']) { echo ' expired'; } ?>">
				<td class="job-name" style="padding-left: <?php echo $indent + 2;?>em">
					<?php
						echo $jobModel->getFormatIcon($job);
						if ($job['Result'] && $job['Result']['id']) {
							echo $html->link($job['Application']['name'] . ' ' . $job['version'] . ' (' . $job['Platform']['name'] . ')', array('controller'=> 'results', 'action'=>'view', $job['Result']['id']));
						} else {
							echo $job['Application']['name'] . ' ' . $job['version'] . ' (' . $job['Platform']['name'] . ')';
						}
					?>
				</td>
				<td>
					<?php
						if (isset($job['Result']['Validator'])) {
							foreach ($job['Result']['Validator'] as $validator) {
								echo $validatorModel->getStateIcon($validator) . ' ';
							}
						}
					?>
				</td>
				<td><?php echo isset($job['Result']['created']) ? $job['Result']['created'] : $job['created']; ?></td>
				<td><?php echo $jobModel->getState($job); ?></td>
				<?php if ($access):?>
					<td class="actions">&nbsp;</td>
				<?php endif;?>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
