<div class="column-right">

	<div class="polaroid">
		<div id="mailinglist">
			<h3>Register for the mailing list</h3>
			<p>If you are interested in this project, why don't you register for the mailing list available for the Officeshots.org project?
			Provide input to the developers, ask for features - that's what its for.</p>
			<p style="text-align:center;"> &raquo; <a href="http://lists.opendocsociety.org/mailman/listinfo/officeshots">register</a></p>
		</div>
	</div>

	<div id="thanks">
		<h3>An initiative of</h3>
		<a href="http://www.noiv.nl"><img src="/img/noiv.png" alt="Nederland in open verbinging logo" /></a><br />
		<a href="http://www.opendocsociety.org"><img src="/img/opendoc.png" alt="OpenDoc Society logo" /></a><br />
		<a href="http://www.nlnet.nl"><img src="/img/nlnet.png" alt="NLNet Foundation logo" /></a><br />
		<a href="http://www.jejik.com"><img src="/img/lonewolves.png" alt="Lone Wolves Foundation logo" /></a><br />
	</div>
</div>
