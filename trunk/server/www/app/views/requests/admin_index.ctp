<div class="requests index">
<h2><?php __('Requests');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th><?php echo $paginator->sort('ip_address');?></th>
	<th><?php echo $paginator->sort('filename');?></th>
	<th><?php __('Jobs');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($requests as $request):?>
	<tr>
		<td class="nowrap">
			<?php echo $html->link($request['Request']['id'], array('action'=>'view', $request['Request']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($request['User']['email_address'], array('controller'=> 'users', 'action'=>'view', $request['User']['id'])); ?>
		</td>
		<td>
			<?php echo inet_dtop($request['Request']['ip_address']); ?>
		</td>
		<td>
			<?php echo $request['Request']['filename']; ?>
		</td>
		<td>
			<?php echo $request['Request']['result_count'] . '/' . $request['Request']['job_count']; ?>
		</td>
		<td>
			<?php echo $request['Request']['created']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $request['Request']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $request['Request']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $request['Request']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->first('<< '.__('first', true).'&nbsp;&nbsp;', array('escape' => false));?>
	<?php echo $paginator->prev('< '.__('previous', true).'&nbsp;&nbsp;', array('escape' => false), null, array('class'=>'disabled', 'escape' => false));?>
   	<?php echo $paginator->numbers();?>&nbsp;&nbsp;
	<?php echo $paginator->next(__('next', true).' >&nbsp;&nbsp;', array('escape' => false), null, array('class'=>'disabled', 'escape' => false));?>
	<?php echo $paginator->last(__('last', true).' >>', array('escape' => false));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Request', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Doctypes', true), array('controller'=> 'doctypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Doctype', true), array('controller'=> 'doctypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
