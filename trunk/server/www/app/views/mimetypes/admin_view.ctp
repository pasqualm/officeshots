<div class="mimetypes view">
<h2><?php  __('Mimetype');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $mimetype['Mimetype']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Icon'); ?></dt>
		<dd>
			<?php echo $html->image('icons/' . $mimetype['Mimetype']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php __('Extension'); ?></dt>
		<dd>
			<?php echo $mimetype['Mimetype']['extension']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Doctype'); ?></dt>
		<dd>
			<?php echo $html->link($mimetype['Doctype']['name'], array('controller'=> 'doctypes', 'action'=>'view', $mimetype['Doctype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php __('Format'); ?></dt>
		<dd>
			<?php echo $html->link($mimetype['Format']['name'], array('controller'=> 'formats', 'action'=>'view', $mimetype['Format']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Mimetype', true), array('action'=>'edit', $mimetype['Mimetype']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Mimetype', true), array('action'=>'delete', $mimetype['Mimetype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $mimetype['Mimetype']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Mimetypes', true), array('action'=>'index')); ?> </li>
	</ul>
</div>
