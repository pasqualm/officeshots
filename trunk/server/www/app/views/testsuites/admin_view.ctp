<div class="testsuites view">
<h2><?php  __('Testsuite');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $testsuite['Testsuite']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Source'); ?></dt>
		<dd>
			<?php echo $testsuite['Testsuite']['source']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Root'); ?></dt>
		<dd>
			<?php echo $testsuite['Testsuite']['root']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Created'); ?></dt>
		<dd>
			<?php echo $testsuite['Testsuite']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Modified'); ?></dt>
		<dd>
			<?php echo $testsuite['Testsuite']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Testsuite', true), array('action'=>'edit', $testsuite['Testsuite']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Testsuite', true), array('action'=>'delete', $testsuite['Testsuite']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $testsuite['Testsuite']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Testsuites', true), array('action'=>'index')); ?> </li>
	</ul>
</div>
